<?php
	session_start();
	$page='view';
	include 'Header.php';
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title></title>
<link rel="stylesheet" type="text/css" href="CSS/Website.css">
        <link rel="stylesheet" type="text/css" 
            media="only screen and (max-width: 480px), only screen and (max-device-width: 480px)" href="CSS/Mobile.css">
        <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">
        </script> -->
	   <script src="Chart.js"></script>
</head>
<body>
	<br>
	<br>
	<br>
	<div class="userinput">
		<h2 class="aligncenter">Student Progress</h2>
		<p>
			<label>Select Student:</label>
			<select name="student"> <!--  onchange="showScore(this.value)"-->
			  <option value="student1">Callie Wilson</option>
			  <option value="student2">Jordan Williams</option>
			  <option value="student3">Caleb Adams</option>
			  <option value="student4">Zoe Johnson</option>
			  <option value="student5">Hailey O'Shea</option>
			  <option value="student6">Natalie Thompson</option>
			  <option value="student7">Aaron Philips</option>
		</select>
		</p>
		<br>	
		<canvas id="score" width="400" height="400"></canvas> 
	</div>
  <script>
		var scoreData = {
			labels : ["Week 1","Week 2","Week 3","Week 4","Week 5","Week 6", "Week 7"],
			datasets : [
			{
				fillColor : "white",
				strokeColor : "#800000",
				pointColor : "#800000",
				pointStrokeColor : "#800000",
				data : [2,3,5,5,7,9,11]
				}
			]
		}
			// Get the context of the canvas element we want to select
			var score = document.getElementById("score").getContext("2d");
			new Chart(score).Line(scoreData);
	   </script>
<script type="text/javascript" charset="utf-8">
<!-- 
	var score_data = {};

	function score_chart() {
		var can = jQuery('#score_chart');
		var ctx = can.get(0).getContext("2d");
		var container = can.parent().parent(); // get width from proper parent

		var $container = jQuery(container);

		can.attr('width', $container.width()); //max width
		can.attr('height', $container.height()); //max height                   

		var chart = new Chart(ctx).Line(score_data);
	}

	var score_data = function () {
		jQuery.post( 'GetScore.php', { 
			action: 'GetScore.php', 
			data: 'Scores',
			security: 'nonce' 
			},
			function( data ) {
				score_data = [
					{
						value: data.status_temp,
						color:"#26292C",
						highlight: "#363B3F",
						label: "Temp"
					},
					{
						value: data.status_pending,
						color:"#FFA500",
						highlight: "#FAD694",
						label: "Pending"
					},
					{
						value: data.status_partial,
						color:"#E14D43",
						highlight: "#FF5A5E",
						label: "Partial"
					},
					{
						value: data.status_complete,
						color: "#76AB48",
						highlight: "#86BC4A",
						label: "Complete"
					}
				];
				score_chart();
			}
		);
	};

	jQuery(document).ready(function($) {
		jQuery(window).resize(score_chart);
		score_data();
	}); -->
</script>
</body>
</html>	<!-- <img src="Images/StudentProgressChart-350.png" width="432px" height="490px" alt="Student Progress Chart"/> -->