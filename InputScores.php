<?php
	$page='input';
 	session_start();
	require 'Header.php';
	include'Connection.php';
?>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="CSS/Website.css">
		<link rel="stylesheet" type="text/css" media=
		"only screen and (max-width: 480px), only screen and (max-device-width: 480px)"
		href="CSS/Mobile.css">
	 <script>
		function outputLvl(lvl) {
		document.querySelector('#lvl').value = lvl;
		}

		function outputRead(words) {
		document.querySelector('#read').value = words;
		}

		function outputWrite(words) {
		document.querySelector('#write').value = words;
		}

		function outputWeek(words) {
		document.querySelector('#week').value = words;
		}
	</script>
</head>
<body>
	<br>
	<br>
	<br>
	<div class="userinput">
		<h2 class="aligncenter">Input Progress</h2>
		 <?php 
			if (isset($_SESSION['success']['score']))
               {
                   echo $_SESSION['success']['score'];
                    unset( $_SESSION['success']['score']);
               }
		?>
		<form method="post" action="SubmitScores.php">
			<!-- <datalist id=words>
				<option>0</option>
				<option>2</option>	
				<option>4</option>
				<option>6</option>
				<option>8</option>
				<option>10</option>
				<option>12</option>
				<option>14</option>
				<option>16</option>
				<option>18</option>
				<option>20</option>
				<option>22</option>
				<option>24</option>
			</datalist> -->
			<p>
			<label>Select Student:</label>
			<select name="student">
			  <option value="student1">Callie Wilson</option>
			  <option value="student2">Jordan Williams</option>
			  <option value="student3">Caleb Adams</option>
			  <option value="student4">Zoe Johnson</option>
			  <option value="student5">Hailey O'Shea</option>
			  <option value="student6">Natalie Thompson</option>
			  <option value="student7">Aaron Philips</option>
		</select>
		</p>
			<p>
                   <label>Reading Level:</label>
			        <input type= "range" min="0" max="25"   list=words name="rlvl" required onchange="outputLvl(value)"> 
				   <output for=rlvl id="lvl"></output>
               </p>
			<p>
                   <label>Written Words:</label>
			        <input type= "range" min="0" max="25" list=words  name="wwords" required onchange="outputWrite(value)">
				   <output for=wwords id="write"></output>
               </p>		 
               <p>
                   <label>Read Words:</label>
			        <input  type= "range" min="0" max="25" list=words  name="rwords" required onchange="outputRead(value)">
				   <output for=rwords id="read" ></output>
				   
               </p>
			 </p>
			<p>
				<label>Week Number:</label>
					 <input  type= "range" min="1" max="25" list=words  name="weekno" required onchange="outputWeek(value)">
				   <output for=weekno id="week" ></output>
			</p>
			<p>
				<label>Progress Date:</label>
					<input type="date" name="repdate">
			</p>
               <button class="accept" type="Submit" value="Submit">Input Progress</button> 
			<button class="cancel" type="Reset" value="Clear" class="selected">Cancel</button>
           </form>
	    </div>
</body>
</html>