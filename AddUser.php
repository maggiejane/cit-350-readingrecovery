<?php
	$page='adduser';
 	session_start();
	require 'Header.php';
?>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="CSS/Website.css">
		<link rel="stylesheet" type="text/css" media=
		"only screen and (max-width: 480px), only screen and (max-device-width: 480px)"
		href="CSS/Mobile.css">
</head>
<body>
	<br>
	<br>
	<br>
	<div class="userinput">
		<h2 class="aligncenter">Add User</h2>
		<?php 
			if (isset($_SESSION['success']['createusr']))
               {
                   echo $_SESSION['success']['createusr'];
                    unset( $_SESSION['success']['createusr']);
               }
		?>
		<form method="post" action="CreateAccount.php">
			<p>
                   <label>First Name:</label>
			        <input type= "text" placeholder="teacher firstname" name="fname" required> 
               </p><p>
                   <label>Last Name:</label>
			        <input type= "text" placeholder="teacher lastname" name="lname" required> 
               </p>		 
              
               <p>
                   <label>Teacher Leader:</label>
			        <input type= "text" placeholder="teacher leader" name="tleader" required> 
               </p>

 			<p>
                   <label>Email:</label>
			        <input type= "email" placeholder="teacheremail@somewhere.edu" name="email" required> 
               </p>
				<!--For Testing Only-->
			<p><label>Password:</label>
			        <input type= "password" placeholder="your password" name="password" required> 
               </p>
			 <p><label>Confirm Password:</label>
			        <input type= "password" placeholder="your password" name="password2" required> 
               </p>
               <button class="accept" type="Submit" value="Submit">Add User</button>
			<button class="cancel" type="Reset" value="Clear" class="selected">Cancel</button> 
           </form>
	    </div>
</body>
</html>