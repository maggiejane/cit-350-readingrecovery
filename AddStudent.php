<?php
	$page='addstudent';
 	session_start();
	require 'Header.php';
?>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="CSS/Website.css">
		<link rel="stylesheet" type="text/css" media=
		"only screen and (max-width: 480px), only screen and (max-device-width: 480px)"
		href="CSS/Mobile.css">
</head>
<body>
		<br>
	<br>
	<br>
	<div class="userinput">
		<h2 class="aligncenter">Add Student</h2>
		<?php 
			if (isset($_SESSION['success']['createstdnt']))
               {
                   echo $_SESSION['success']['createstdnt'];
                    unset( $_SESSION['success']['createstdnt']);
               }
		?>
		<form method="post" action="CreateStudent">
			<p>
                   <label>First Name:</label>
			        <input type= "text" placeholder="Student firstname" name="sfname" required> 
               </p><p>
                   <label>Last Name:</label>
			        <input type= "text" placeholder="Student lastname" name="slname" required> 
               </p>		 
               <button  class="accept" type="Submit" value="Submit">Add Student</button>
			<button class="cancel"  type="Reset" value="Clear">Cancel</button>
           </form>
	    </div>
</body>
</html>