<?php
    $page='getaccount';    
    session_start();
	include 'Header.php';
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title></title>

	<link rel="stylesheet" type="text/css" href="CSS/Website.css">
	<link rel="stylesheet" type="text/css" 
		media="only screen and (max-width: 480px), only screen and (max-device-width: 480px)" href="CSS/Mobile.css">

</head>
<body>
	<br>
	<br>
	<div class="userinput">
		<h2 class="aligncenter">Request Account</h2>
		<form method="post" action="CreateAccount.php">
			<p>
                   <label>First Name:</label>
			        <input type= "text" placeholder="your firstname" name="fname" required> 
               </p><p>
                   <label>Last Name:</label>
			        <input type= "text" placeholder="your lastname" name="lname" required> 
               </p>		 
               <p>
                   <label>Email:</label>
			        <input type= "email" placeholder="youremail@somewhere.edu" name="email" required> 
               </p>
			 <p><label>Teacher Leader:</label>
			        <input type= "text" placeholder="your teacher leader" name="tleader" required> 
               </p>
			<!--For Testing Only-->
               <p><label>Password:</label>
			        <input type= "password" placeholder="your password" name="password" required> 
               </p>
			 <p><label>Confirm Password:</label>
			        <input type= "password" placeholder="your password" name="password2" required> 
               </p>
               <button class="accept" type="Submit" value="Submit">Request Account</button>
			<button class="cancel" type="Reset" value="Clear" class="selected">Cancel</button> 
           </form>
	    </div>
</body>
</html>