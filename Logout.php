<?php
    $page='logout';
    include 'Header.php';
    session_start();

        //logout
    if ($_SESSION['loggedin']==true)
    {
        $_SESSION['success']=array('logout'=>'<p class="success aligncenter logout">Logout Successful.</p>');
        echo $_SESSION['success']['logout'];
        session_unset();
        session_destroy();

	   //starts an empty session
	   session_start();
        $_SESSION['loggedin'] = false;
	   $_SESSION['usertype']='guest';               
    }

    else
    {
    	   $_SESSION['usertype']='guest';
        $_SESSION['error']=array('notloggedin'=>'<p class="error aligncenter"> Error: Please login.</p>');
        header("location:Login.php");
    }   
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Logout</title>
        <link rel="stylesheet" type="text/css" href="CSS/Website.css">
		<link rel="stylesheet" type="text/css" media=
		"only screen and (max-width: 480px), only screen and (max-device-width: 480px)"
		href="CSS/Mobile.css">
    </head>
    <body>
    </body>
</html>